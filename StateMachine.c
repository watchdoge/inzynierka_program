/*
 * adc.c
 *
 *  Created on: 30-11-2016
 *      Author: Adam
 */

/*======================================================================================*/
/*                       ####### PREPROCESSOR DIRECTIVES #######                        */
/*======================================================================================*/
/*---------------------- INCLUDE DIRECTIVES FOR STANDARD HEADERS -----------------------*/

/*----------------------- INCLUDE DIRECTIVES FOR OTHER HEADERS -------------------------*/

#include "includes/StateMachine.h"
#include "avr/io.h"

/*--------------------------- LOCAL DEFINES FOR CONSTANTS ------------------------------*/

/*------------------------------- LOCAL DEFINE MACROS ----------------------------------*/

/*======================================================================================*/
/*                      ####### LOCAL TYPE DECLARATIONS #######                         */
/*======================================================================================*/
/* ------------------------------------ ENUMS ----------------------------------------- */

/*------------------------------------ STRUCT ------------------------------------------*/

/*------------------------------------ UNIONS ------------------------------------------*/

/*-------------------------------- OTHER TYPEDEFS --------------------------------------*/

/*======================================================================================*/
/*                         ####### OBJECT DEFINITIONS #######                           */
/*======================================================================================*/
/*------------------------------- EXPORTED OBJECTS -------------------------------------*/

/*-------------------------------- LOCAL OBJECTS ---------------------------------------*/

volatile uint16_t ErrReg = 1;
volatile uint8_t BufferPointer = 0;
volatile uint8_t StopFlag = 0;

/*
 * TODO :brief
 * */
void Machine(void)
{
	/* Creating variables for StateMachine */
	uint16_t StateRegister = IDLE;

	while(1)
	{
		switch(StateRegister & 0b00000111)
		{

		case IDLE:

		/* Checking if error occurred */
		if(ErrReg != 1)
		{
			ErrorHandler();

			StateMachine_Clear(&StateRegister);
		}

		/* Checking if CommandFrame is ready */
		if(FrameReadyFlag == 1)
		{
			ErrReg = BufferHandler(&StateRegister);

		}


			/* Breaking case */
			break;

		case SETTINGS:

			/* Handling settings */
			SettingsHandler(StateRegister);

			/* Clearing data in StateMachine */
			StateMachine_Clear(&StateRegister);

			/* Breaking case */
			break;

		case MEASUREMENTS:

			/* Handling measurements */
			MeasurementsHandler(StateRegister);

			/* Clearing data in StateMachine */
			StateMachine_Clear(&StateRegister);

			/* Breaking case */
			break;

		default:
			break;

		}
	}
}

/*
 * TODO :brief
 * */
uint8_t BufferHandler(uint16_t *StateReg)
{

	/* Creating operational buffer */
	uint8_t buff = 0;

	/* Setting BufferPointer to begin of frame */
	BufferPointer = StartPoint;

	/* Checking correction of frame */
	if(RxBuff[BufferPointer] != ':')
	{
		/* Returning error */
		return 2;
	}

	/* Incrementing BufferPointer */
	BufferPointer++;

	/* Checking if number for 1st lvl of menu is in proper range xeN: <0,7> */
	if(   (RxBuff[BufferPointer] < 0x30)  |   (RxBuff[BufferPointer] > 0x37)   )
	{
		return 2;
	}

	/* Writing value of 1st lvl of menu to state Register*/
	*StateReg = (uint16_t)(0b111 & (RxBuff[BufferPointer] - 0x30));

	/* Incrementing BufferPointer */
	BufferPointer++;

	/* Reading commands if read sing is not a frame end sign or separator */
	if(   !(RxBuff[BufferPointer] == '*') & !(RxBuff[BufferPointer] == '/') )
	{
		buff = (uint8_t)RxBuff[BufferPointer] - (uint8_t)0x30;

		/* Incrementing BufferPointer */
		BufferPointer++;
	}
	else
	{
		return 2;
	}

	/* Reading commands if read sing is not a frame end sign or separator */
	if(   !(RxBuff[BufferPointer] == '*') & !(RxBuff[BufferPointer] == '/') )
	{
		/* if second digit exist, first must be miltiplied by 10 */
		buff *= 10;

		/* Reading second value for second lvl menu */
		buff += (uint8_t)RxBuff[BufferPointer] - (uint8_t)0x30;
	}
		/* Placing read shifted 2nd lvl menu value to StateRegister */
	*StateReg |= (uint16_t)buff << 3;

	/* Incrementing BufferPointer */
	BufferPointer++;

	return 1;
}

/*
 * TODO :brief
 * */
void SettingsHandler(uint16_t StateReg)
{

	switch(StateReg >> 3)
	{

	case 1:

		/* Opening valve 1 */
			VALVECONN(VALVE1);

		/* Switching on LED 1 */
			LEDON(LED1);

		/* Breaking a case */
		break;

	case 2:

		/* Opening valve 2 */
			VALVECONN(VALVE2);

		/* Switching on LED 2 */
			LEDON(LED2);

		/* Breaking a case */
		break;

	case 3:

		/* Opening valve 3 */
			VALVECONN(VALVE3);

		/* Switching on LED 3 */
			LEDON(LED3);

		/* Breaking a case */
		break;

	case 4:

		/* Closing valve 1 */
			VALVEDISCONN(VALVE1);

		/* Switching off LED 1 */
			LEDOFF(LED1);

		/* Breaking a case */
		break;

	case 5:

		/* Closing valve 2 */
			VALVEDISCONN(VALVE2);

		/* Switching off LED 2 */
			LEDOFF(LED2);

		/* Breaking a case */
		break;

	case 6:

		/* Closing valve 3 */
			VALVEDISCONN(VALVE3);

		/* Switching off LED 3 */
			LEDOFF(LED3);

		/* Breaking a case */
		break;

	case 7:

		ErrReg = RegulationConfig();

		break;

	}

}
/*
 * TODO :brief
 * */
void MeasurementsHandler(uint16_t StateReg)
{
	switch(StateReg >> 3)
	{
	case 1:
		wynik = read_adc(1);

		USART_Transmit((unsigned char)'\r');
		USART_Transmit((unsigned char)'\n');
		USART_Transmit_int16_t(wynik);
		break;

	case 2:
		StopFrameHandlerConfig();
		StopFrameHandlerEnable();

		while(StopFlag == 0)
		{
		wynik = read_adc(1);

		USART_Transmit((unsigned char)'\r');
		USART_Transmit((unsigned char)'\n');
		USART_Transmit_int16_t(wynik);

		_delay_ms(20);
		}

		StopFlag = 0;
		StopFrameHandlerDisable();
		break;
	default:
		break;
	}
}

/*
 * TODO :brief
 * */
void ErrorHandler(void)
{
	switch(ErrReg)
	{
	case 2:
		USART_Transmit('2');
		USART_Transmit('\r');
		USART_Transmit('\n');
	ErrReg = 1;
		break;

	case 3:
		USART_Transmit('3');
		USART_Transmit('\r');
		USART_Transmit('\n');
	ErrReg = 1;
		break;

	case 4:
		USART_Transmit('4');
		USART_Transmit('\r');
		USART_Transmit('\n');
	ErrReg = 1;
		break;

	case 5:
		USART_Transmit('5');
		USART_Transmit('\r');
		USART_Transmit('\n');
	ErrReg = 1;
		break;

	case 6:
		USART_Transmit('6');
		USART_Transmit('\r');
		USART_Transmit('\n');
	ErrReg = 1;
		break;
	default:
		USART_Transmit('d');
		USART_Transmit('\r');
		USART_Transmit('\n');
	ErrReg = 1;
		break;
	}
}

/*
 * TODO :brief
 * */
void StateMachine_Clear(uint16_t *StateReg)
{
	Clear_Buffer();

	FrameReadyFlag = 0;

	BufferPointer = StartPoint;

	*StateReg = 1;

}

/*
 * @brief	Configures interrupt from Timer 0 for Sampling
 * @param	None
 * @retval	None
 * */
void ADC_sampling_config(void)
{
	/* Enabling CTC mode on Timer0 */
	TCCR0A |= (1<<WGM01);

	/* Setting prescaler to 8 */
	TCCR0B |= (1<<CS01);// DEBUG | (1<<CS00);

	/* Number of increments to interrupt */
	OCR0A = 40;
}

/*
 * @brief	Handles Interrupt from Timer and makes adc measurement
 * @param	None
 * @retval	None
 * */
void ADC_sampling_Handler(void)
{
	/* Reading  value from adc */
	FastRead_1_ch();
}

/*
 * TODO :brief
 * */
void ADC_sampling_Enable(void)
{
	/* Enabling Compare interrupt on Timer0 */
	TIMSK0 |= (1<<OCIE0A);

}

/*
 * TODO :brief
 * */
void ADC_sampling_Disable(void)
{
	/* Enabling Compare interrupt on Timer0 */
	TIMSK0 &= ~(1<<OCIE0A);
}

/*
 * @brief	Configures interrupt from Timer 1 for StopFrameHandler
 * @param	None
 * @retval	None
 * */
void StopFrameHandlerConfig(void)
{

	/* Clearing Registers from previous settings */
	TCCR1A = 0;
	TCCR1B = 0;

	/* Setting 0 value for timer initialization */
	TCNT1  = 0;

	/* set up timer with prescaler = 256 and timer in CTC mode(mode 4) */
	TCCR1B |= (1 << CS12) | (1<<WGM12);

    /* Sets value for compare, interrupt every 500ms */
    OCR1A = 31250;
}

/*
 * @brief	Handles interrupt from Timer compare
 * @param 	None
 * @retval	None
 * */
void StopFrameHandler(void)
{
	/* Checking if Frame is Ready */
	if(FrameReadyFlag == 1)
	{
		/* Setting BufferPointer to begin of frame */
		BufferPointer = StartPoint;

		/* Checking if command is StopCommand */
		if ((RxBuff[BufferPointer] == ':') & (RxBuff[BufferPointer+1] == '*') )
		{
			/* Setting StopFlag */
			StopFlag = 1;
		}
	}
}

/*
 * @brief	Enables Timer interrupt for StopFrameHandler
 * @param	None
 * @retval	None
 * */
void StopFrameHandlerEnable(void)
{
    /* Enabling overflow interrupt */
    TIMSK1 |= (1 << OCIE1A);
}

/*
 * @brief	Disables Timer interrupt for StopFrameHandler
 * @param	None
 * @retval	None
 * */
void StopFrameHandlerDisable(void)
{
    // disable overflow interrupt
    TIMSK1 &= ~(1 << OCIE1A);
}

uint16_t RegulationConfig(void)
{
	uint16_t histplus = 0;
	uint16_t histminus = 0;
	uint16_t Expectedpressure;

	BufferPointer--;

	if(RxBuff[BufferPointer] != '/')
	{
		return 4;
	}
	Receive_int_2x8(&histminus);



	if(RxBuff[BufferPointer] != '/')
	{
		return 5;
	}

	Receive_int_2x8(&histplus);


	if(RxBuff[BufferPointer] != '/')
	{
		return 6;
	}

	Receive_int_2x8(&Expectedpressure);

	ADC_sampling_config();
	StopFrameHandlerConfig();

	StopFrameHandlerEnable();
	ADC_sampling_Enable();

	VALVEDISCONN(VALVE1 | VALVE2 | VALVE3);

	ErrReg = Regulation(histplus,histminus,Expectedpressure);

	VALVEDISCONN(VALVE1 | VALVE2 | VALVE3);

	StopFlag = 0;

	ADC_sampling_Disable();
	StopFrameHandlerDisable();

	return ErrReg;
}

uint16_t Regulation(uint16_t HistPlus, uint16_t HistMinus, uint16_t ExpectedPressure)
{
	if(StopFlag == 1)
	{
		return 3;
	}

	while(1)
	{

		if(wynik > (ExpectedPressure + HistPlus))
		{


			VALVECONN(VALVE2);

			while(wynik > (ExpectedPressure + HistPlus))
			{
				if(StopFlag == 1)
				{
					return 3;
				}
			}

			VALVEDISCONN(VALVE2);

			if(StopFlag == 1)
			{
				return 3;
			}

			if(PIND & (VALVE3))
			{

				while((wynik < (ExpectedPressure + HistPlus)) & (wynik > (ExpectedPressure - HistMinus)))
				{
					if(StopFlag == 1)
					{
						return 3;
					}
				}

			}
			else
			{
				VALVECONN(VALVE3);

				while((wynik < (ExpectedPressure + HistPlus)) & (wynik > (ExpectedPressure - HistMinus)))
				{
					if(StopFlag == 1)
					{
						return 3;
					}
				}
			}

		}
		else
		{

			if(wynik < (ExpectedPressure - HistMinus))
			{
				VALVECONN(VALVE1);

				while(wynik < (ExpectedPressure - HistMinus))
				{
					if(StopFlag == 1)
					{
						return 3;
					}
				}

				VALVEDISCONN(VALVE1);

				if(PIND & VALVE3)
				{

					while((wynik < (ExpectedPressure + HistPlus)) & (wynik > (ExpectedPressure - HistMinus)))
					{
						if(StopFlag == 1)
						{
							return 3;
						}
					}

				}
				else
				{
					VALVECONN(VALVE3);

					while((wynik < (ExpectedPressure + HistPlus)) & (wynik > (ExpectedPressure - HistMinus)))
					{
						if(StopFlag == 1)
						{
							return 3;
						}
					}

				}

			}
			else
			{
				if(PIND & VALVE3)
				{

					while((wynik < (ExpectedPressure + HistPlus)) & (wynik > (ExpectedPressure - HistMinus)))
					{
						if(StopFlag == 1)
						{
							return 3;
						}
					}

				}
				else
				{
					VALVECONN(VALVE3);

					while((wynik < (ExpectedPressure + HistPlus)) & (wynik > (ExpectedPressure - HistMinus)))
					{
						if(StopFlag == 1)
						{
							return 3;
						}
					}

				}
			}

		}

	}
	return 1;
}

/*
 * TODO:
 * @brief
 * @param
 * @retval
 * */
void Receive_int_2x8( uint16_t *buff)
{
	uint16_t mltp = 10;

	*buff = 0;

	BufferPointer++;

	while((RxBuff[BufferPointer] != '*') & (RxBuff[BufferPointer] != '/'))
	{
		/*DEBUG*/
		USART_Transmit(RxBuff[BufferPointer]);
		USART_Transmit_endl();

		USART_Transmit_int16_t(mltp);
		USART_Transmit_endl();

		USART_Transmit_int16_t(*buff);
		USART_Transmit_endl();

	*buff *=  mltp;

	*buff += (uint16_t)(RxBuff[BufferPointer] - 0x30);

	BufferPointer++;

	}
}

/*======================================================================================*/
/*                    ####### LOCAL FUNCTIONS PROTOTYPES #######                        */
/*======================================================================================*/

/*======================================================================================*/
/*          ####### LOCAL INLINE FUNCTIONS AND FUNCTION-LIKE MACROS #######             */
/*======================================================================================*/

/*======================================================================================*/
/*                   ####### LOCAL FUNCTIONS DEFINITIONS #######                        */
/*======================================================================================*/

