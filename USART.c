/*
 * adc.c
 *
 *  Created on: 30-11-2016
 *      Author: Adam
 */

/*======================================================================================*/
/*                       ####### PREPROCESSOR DIRECTIVES #######                        */
/*======================================================================================*/
/*---------------------- INCLUDE DIRECTIVES FOR STANDARD HEADERS -----------------------*/

/*----------------------- INCLUDE DIRECTIVES FOR OTHER HEADERS -------------------------*/

#include "includes/USART.h"
#include "avr/io.h"
#include "stdio.h"

/*--------------------------- LOCAL DEFINES FOR CONSTANTS ------------------------------*/
volatile uint8_t RxBuff[RXBUFFSIZE];
volatile uint8_t StartPoint = 0;
volatile uint8_t EndPoint = 0;
volatile uint8_t FrameReadyFlag = 0;
volatile uint8_t RxCounter = 0;
/*------------------------------- LOCAL DEFINE MACROS ----------------------------------*/

/*======================================================================================*/
/*                      ####### LOCAL TYPE DECLARATIONS #######                         */
/*======================================================================================*/
/* ------------------------------------ ENUMS ----------------------------------------- */

/*------------------------------------ STRUCT ------------------------------------------*/

/*------------------------------------ UNIONS ------------------------------------------*/

/*-------------------------------- OTHER TYPEDEFS --------------------------------------*/

/*======================================================================================*/
/*                         ####### OBJECT DEFINITIONS #######                           */
/*======================================================================================*/
/*------------------------------- EXPORTED OBJECTS -------------------------------------*/

/*-------------------------------- LOCAL OBJECTS ---------------------------------------*/

/*======================================================================================*/
/*                    ####### LOCAL FUNCTIONS PROTOTYPES #######                        */
/*======================================================================================*/

/*======================================================================================*/
/*          ####### LOCAL INLINE FUNCTIONS AND FUNCTION-LIKE MACROS #######             */
/*======================================================================================*/

/*======================================================================================*/
/*                   ####### LOCAL FUNCTIONS DEFINITIONS #######                        */
/*======================================================================================*/
void USART_Init(unsigned int ubrr)
{
	/*Set baud rate */
	UBRR0H = (unsigned char)(ubrr>>8);
	UBRR0L = (unsigned char)ubrr;

	/* Enable receiver and transmitter and enable interrupt for receiving */
	UCSR0B = (1<<RXEN0) | (1<<TXEN0) | (1<<RXCIE0);

	/* Set frame format: 8data, 1 stop bit */
	UCSR0C = (1<<UCSZ01)| (1 << UCSZ00);
}

void USART_Transmit( unsigned char data )
{
/* Wait for empty transmit buffer */
while ( !( UCSR0A & (1<<UDRE0)) );

/* Put data into buffer, sends the data */
UDR0 = data;
}

/*
 * @brief:		Transforms uint8_t value into a string and sends through UART
 * @param:		data - data to be transmitted
 * @retval:		None
 * */
void USART_Transmit_int8_t(uint8_t data)
{
	char str[3];
	uint8_t cnt = 0;

	sprintf(str, "%d", data);
	while(str[cnt] != 0)
	{
		USART_Transmit((unsigned char)str[cnt]);
		cnt++;
	}
}

/*
 * @brief:		Transforms uint16_t value into a string and sends through UART
 * @param:		data - data to be transmitted
 * @retval:		None
 * */
void USART_Transmit_int16_t(uint16_t data)
{
	char str[16];
	uint8_t cnt = 0;
	sprintf(str, "%u", data);

	while(str[cnt] != 0)
	{
		USART_Transmit((unsigned char)str[cnt]);
		cnt++;
	}
}

/*
 * @brief	Receiving 1 byte of data
 * @param	None
 * @retval	None
 * */
void USART_Receive( uint8_t *buff )
{
	/* Wait for data to be received */
	while ( !(UCSR0A & (1<<RXC0)) )
	;
	/* Get and return received data from buffer */
	*buff = UDR0;
}

/*
 * @brief	Interrupt handler from USART
 * @param	None
 * @retval	None
 * */
void USART_INT_Handler(void)
{
	/* Checking if frame is received */
	if((UCSR0A & (1<<RXC0)))
	{
		/* Checking if buffer is not full */
		if(RxCounter < RXBUFFSIZE)
		{
			/* Writing received data to buffer */
			USART_Receive(&RxBuff[RxCounter]);

			/*DEBUG*/
			USART_Transmit((unsigned char)RxBuff[RxCounter]);

			/* Checking if received sign is frame start sign */
			if (RxBuff[RxCounter] == ':')
			{
				/* Setting Startpointer */
				StartPoint = RxCounter;
			}
			/* Checking if received sign is EndPoint  */
			else if(RxBuff[RxCounter] == '*')
			{
				/* Setting EndPointer */
				EndPoint = RxCounter;

				/* Setting FrameReadyFlag */
				FrameReadyFlag = 1;
			}

		/* Incrementing Rxbuffer counter */
		RxCounter++;
		}
	}
}

void USART_Transmit_endl()
{
	USART_Transmit('\r');
	USART_Transmit('\n');
}

/*
 * @brief: 	Clears USART buffer and sets start and End porint to 0
 * @param: 	None
 * @retval: None
 * */
void Clear_Buffer(void)
{
	uint8_t cnt = 0;
	/* Clearing Buffer */
	for (cnt = 0 ; cnt < RXBUFFSIZE ; cnt++)
	{
		RxBuff[cnt] = 0;
	}

	/* Resets StartPoint */
	StartPoint = 0;

	/* Resets EndPoint */
	EndPoint = 0;

	/* Clearing RxCounter */

	RxCounter = 0;
}
