/*
 * adc.c
 *
 *  Created on: 30-11-2016
 *      Author: Adam
 */

/*======================================================================================*/
/*                       ####### PREPROCESSOR DIRECTIVES #######                        */
/*======================================================================================*/
/*---------------------- INCLUDE DIRECTIVES FOR STANDARD HEADERS -----------------------*/

/*----------------------- INCLUDE DIRECTIVES FOR OTHER HEADERS -------------------------*/

#include "includes/adc.h"
#include "avr/io.h"

/*--------------------------- LOCAL DEFINES FOR CONSTANTS ------------------------------*/

/*------------------------------- LOCAL DEFINE MACROS ----------------------------------*/

/*======================================================================================*/
/*                      ####### LOCAL TYPE DECLARATIONS #######                         */
/*======================================================================================*/
/* ------------------------------------ ENUMS ----------------------------------------- */

/*------------------------------------ STRUCT ------------------------------------------*/

/*------------------------------------ UNIONS ------------------------------------------*/

/*-------------------------------- OTHER TYPEDEFS --------------------------------------*/

/*======================================================================================*/
/*                         ####### OBJECT DEFINITIONS #######                           */
/*======================================================================================*/
/*------------------------------- EXPORTED OBJECTS -------------------------------------*/

/*-------------------------------- LOCAL OBJECTS ---------------------------------------*/

/*======================================================================================*/
/*                    ####### LOCAL FUNCTIONS PROTOTYPES #######                        */
/*======================================================================================*/

/*======================================================================================*/
/*          ####### LOCAL INLINE FUNCTIONS AND FUNCTION-LIKE MACROS #######             */
/*======================================================================================*/

/*======================================================================================*/
/*                   ####### LOCAL FUNCTIONS DEFINITIONS #######                        */
/*======================================================================================*/

/*
 * @brief:		Initialization of ADC for pressure measurements
 * @param:		None
 * @retval:		None
 * */
void ADC_Init(void){

	 /* Setting prescaler for ADC clock 16000000/128 = 125000Hz */
	 //ADCSRA |= ((1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0));    //16Mhz/128 = 125Khz the ADC reference clock

	 /* Setting reference to external AVCC (5V) */
	 ADMUX |= (1<<REFS0);

	 /* Enabling ADC */
	 ADCSRA |= (1<<ADEN);

	 /* First conversion is slow so need to be done to ensure proper functionality */
	 ADCSRA |= (1<<ADSC);

}

uint16_t read_adc(uint8_t channel)
{
	 /* Clearing channel used before */
	 ADMUX &= 0xF0;

	 /* Sets new channel to be read */
	 ADMUX |= channel;

	 /* Begins a conversion */
	 ADCSRA |= (1<<ADSC);

	 /* Wait while conversion is done */
	 while(ADCSRA & (1<<ADSC));

	 /* Returning measured value */
	 return ADCW;
}


void FastRead_1_ch(void)
{
	 /* Sets new channel to be read */
	 ADMUX = 1;

	 /* Begins a conversion */
	 ADCSRA |= (1<<ADSC);

	 /* Wait while conversion is done */
	 while(ADCSRA & (1<<ADSC));

	 /* Returning measured value */
	 wynik = ADCW;
}


