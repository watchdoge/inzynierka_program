/*
 * adc.h
 *
 *  Created on: 30-11-2016
 *      Author: Adam
 */

/*----------------------- DEFINE TO PREVENT RECURSIVE INCLUSION ------------------------*/
#ifndef STATEMACHINE_HPP_
#define STATEMACHINE_HPP_

/*======================================================================================*/
/*                       ####### PREPROCESSOR DIRECTIVES #######                        */
/*======================================================================================*/
/*---------------------- INCLUDE DIRECTIVES FOR STANDARD HEADERS -----------------------*/

/*----------------------- INCLUDE DIRECTIVES FOR OTHER HEADERS -------------------------*/
#include "avr/io.h"
#include "USART.h"
#include "main.h"
#include "adc.h"
#include "util/delay.h"
/*-------------------------- EXPORTED DEFINES FOR CONSTANTS ----------------------------*/
#define IDLE			(uint8_t)1
#define SETTINGS		(uint8_t)2
#define MEASUREMENTS	(uint8_t)3

/*------------------------------------ UNIONS ------------------------------------------*/

/*-------------------------------- OTHER TYPEDEFS --------------------------------------*/

/*======================================================================================*/
/*                    ####### EXPORTED OBJECT DECLARATIONS #######                      */
/*======================================================================================*/
extern volatile uint16_t ErrReg;
extern volatile uint8_t BufferPointer;
extern volatile uint8_t StopFlag;
/*======================================================================================*/
/*                   ####### EXPORTED FUNCTIONS PROTOTYPES #######                      */
/*======================================================================================*/
void Machine(void);
uint8_t BufferHandler(uint16_t *StateReg);
void Receive_int_2x8( uint16_t *buff);

void SettingsHandler(uint16_t StateReg);
void MeasurementsHandler(uint16_t StateReg);
void ErrorHandler(void);
void StateMachine_Clear(uint16_t *StateReg);

/* Regulation mode functions */
uint16_t RegulationConfig(void);
uint16_t Regulation(uint16_t HistPlus, uint16_t HistMinus, uint16_t ExpectedPressure);



void ADC_sampling_config(void);
void ADC_sampling_Handler(void);
void ADC_sampling_Enable(void);
void ADC_sampling_Disable(void);

void StopFrameHandlerConfig(void);
void StopFrameHandler(void);
void StopFrameHandlerEnable(void);
void StopFrameHandlerDisable(void);

/*======================================================================================*/
/*         ####### EXPORTED INLINE FUNCTIONS AND FUNCTION-LIKE MACROS #######           */
/*======================================================================================*/


#endif /* STATE_MACHINE_HPP_ */


/*=======================================================================================*
 *                       ####### FILE REVISION HISTORY #######                           *
 *---------------------------------------------------------------------------------------*
 *  Date        Rev     Name        Summary of changes                                   *
 *  dd-mm-yy    xx.yy                                                                    *
 *---------------------------------------------------------------------------------------*
 *
 *  30-10-16    0.1     DPA         Initial revision
 *
 *=======================================================================================*/
