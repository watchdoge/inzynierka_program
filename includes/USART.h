/*
 * USART.h
 *
 *  Created on: 01-12-2016
 *      Author: Adam
 */

#ifndef USART_H_
#define USART_H_
/*======================================================================================*/
/*                       ####### PREPROCESSOR DIRECTIVES #######                        */
/*======================================================================================*/
/*---------------------- INCLUDE DIRECTIVES FOR STANDARD HEADERS -----------------------*/

/*----------------------- INCLUDE DIRECTIVES FOR OTHER HEADERS -------------------------*/
#include "avr/io.h"
/*-------------------------- EXPORTED DEFINES FOR CONSTANTS ----------------------------*/

#define FOSC 16000000 // Clock Speed
#define BAUD 9600
#define MYUBRR (FOSC/16/BAUD)-1
#define RXBUFFSIZE 128

/*------------------------------------ UNIONS ------------------------------------------*/

/*-------------------------------- OTHER TYPEDEFS --------------------------------------*/

/*======================================================================================*/
/*                    ####### EXPORTED OBJECT DECLARATIONS #######                      */
/*======================================================================================*/
/* creating some global variables */
extern volatile uint8_t FrameReadyFlag;
extern volatile uint8_t StartPoint;
extern volatile uint8_t EndPoint;
extern volatile uint8_t RxBuff[RXBUFFSIZE];
/*======================================================================================*/
/*                   ####### EXPORTED FUNCTIONS PROTOTYPES #######                      */
/*======================================================================================*/

void USART_Init(unsigned int ubrr);
void USART_Transmit( unsigned char data );
void USART_Transmit_int8_t(uint8_t data);
void USART_Transmit_int16_t(uint16_t data);
void USART_Transmit_endl();
void USART_Receive( uint8_t *buff );
void USART_INT_Handler(void);
void Clear_Buffer(void);


/*======================================================================================*/
/*         ####### EXPORTED INLINE FUNCTIONS AND FUNCTION-LIKE MACROS #######           */
/*======================================================================================*/

#endif /* USART_H_ */

/*=======================================================================================*
 *                       ####### FILE REVISION HISTORY #######                           *
 *---------------------------------------------------------------------------------------*
 *  Date        Rev     Name        Summary of changes                                   *
 *  dd-mm-yy    xx.yy                                                                    *
 *---------------------------------------------------------------------------------------*
 *
 *  30-10-16    0.1     DPA         Initial revision
 *
 *=======================================================================================*/
