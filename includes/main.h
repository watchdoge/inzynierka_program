/*
 * adc.h
 *
 *  Created on: 30-11-2016
 *      Author: Adam
 */

/*----------------------- DEFINE TO PREVENT RECURSIVE INCLUSION ------------------------*/

#ifndef MAIN_H_
#define MAIN_H_

/*======================================================================================*/
/*                       ####### PREPROCESSOR DIRECTIVES #######                        */
/*======================================================================================*/
/*---------------------- INCLUDE DIRECTIVES FOR STANDARD HEADERS -----------------------*/

/*----------------------- INCLUDE DIRECTIVES FOR OTHER HEADERS -------------------------*/
#include "avr/io.h"
/*-------------------------- EXPORTED DEFINES FOR CONSTANTS ----------------------------*/

#define LEDON(LED)			PORTC |= LED
#define LEDOFF(LED)			PORTC &= ~(LED)

#define LED1				(1<<PC2)
#define LED2				(1<<PC3)
#define LED3				(1<<PC4)
#define LED4				(1<<PC5)

#define VALVECONN(VALVE)	PORTD |= VALVE
#define VALVEDISCONN(VALVE)	PORTD &= ~(VALVE)

#define VALVE1				(1<<PD5)
#define VALVE2				(1<<PD7)
#define VALVE3				(1<<PD6)





/*------------------------------------ UNIONS ------------------------------------------*/

/*-------------------------------- OTHER TYPEDEFS --------------------------------------*/

/*======================================================================================*/
/*                    ####### EXPORTED OBJECT DECLARATIONS #######                      */
/*======================================================================================*/
extern volatile uint16_t wynik;
/*======================================================================================*/
/*                   ####### EXPORTED FUNCTIONS PROTOTYPES #######                      */
/*======================================================================================*/

/*======================================================================================*/
/*         ####### EXPORTED INLINE FUNCTIONS AND FUNCTION-LIKE MACROS #######           */
/*======================================================================================*/

/*=======================================================================================*
 *                       ####### FILE REVISION HISTORY #######                           *
 *---------------------------------------------------------------------------------------*
 *  Date        Rev     Name        Summary of changes                                   *
 *  dd-mm-yy    xx.yy                                                                    *
 *---------------------------------------------------------------------------------------*
 *
 *  30-10-16    0.1     DPA         Initial revision
 *
 *=======================================================================================*/

#endif /* MAIN_H_ */
