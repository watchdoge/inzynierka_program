/*
 * main.c
 *
 *  Created on: 13-12-2014
 *      Author: Adam
 */

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "includes/adc.h"
#include "includes/USART.h"
#include "includes/StateMachine.h"
#include "includes/main.h"

volatile uint16_t wynik = 0;

/*
 * Handling interrupt from TIMER0 CTC
 * */
ISR(TIMER0_COMPA_vect)
{
	ADC_sampling_Handler();
}

/*
 * Handling interrupt from TIMER1 CTC
 * */
ISR(TIMER1_COMPA_vect)
{
	StopFrameHandler();
}

/*
 * Handling interrupt from USART
 * */
ISR(USART_RX_vect)
{
	/* Handling interrupt from receiving byte through USART */
	USART_INT_Handler();
}

int main(void)
{
	/* ---------------------Inicializacje peryferi�w--------------------- */

	/* Inicializacje pin�w do kt�rych pod��czone s� diody */
	DDRC |= ( LED1 | LED2 | LED3 | LED4);
	DDRD |= ( VALVE1 | VALVE2 | VALVE3);
	LEDON(LED1|LED2|LED3|LED4);

	/* Inicializacja ADC */
	ADC_Init();

	/* Inicializacja USART0 */
	USART_Init(MYUBRR);

	/* Globalne zezwolenie na przerwania */
	sei();

	_delay_ms(100);

	LEDOFF(LED1|LED2|LED3|LED4);


	while(1)
	{
		/* W��czenie maszyny stan�w */
		Machine();
	}
}

